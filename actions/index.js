import fetch from 'isomorphic-fetch'

export const REQUEST_POSTS = 'REQUEST_POSTS'
export const RECEIVE_POSTS = 'RECEIVE_POSTS'
export const SELECT_USER = 'SELECT_USER'
export const INVALIDATE_USER = 'INVALIDATE_USER'
export const RECEIVE_USERS = 'RECEIVE_USERS'

export function selectUser(user) {
  return {
    type: SELECT_USER,
    user
  }
}

export function invalidateUser(user) {
  return {
    type: INVALIDATE_USER,
    user
  }
}

function requestPosts(user) {
  return {
    type: REQUEST_POSTS,
    user
  }
}

function receivePosts(user, json) {
  return {
    type: RECEIVE_POSTS,
    user,
    posts: json.data.children.map(child => child.data),
    receivedAt: Date.now()
  }
}

function receiveUsers(json) {
  console.log("@@@@@@@@@@@", json)
  return {
    type: RECEIVE_USERS,
    users: json.hits,
    receivedAt: Date.now()
  }
}

export function fetchUsers(json) {
  return dispatch => {
    return fetch(`http://cyborg-api.default/api/user`)
    .then(resp => resp.json())
    .then(json => dispatch(receiveUsers(json)))
  }
  return {
    type: RECEIVE_USERS,
    users: json.data.children.map(child => child.data),
    receivedAt: Date.now()
  }
}

function fetchPosts(user) {
  return dispatch => {
    dispatch(requestPosts(user))
    return fetch(`https://www.reddit.com/r/${user}.json`)
      .then(response => response.json())
      .then(json => dispatch(receivePosts(user, json)))
  }
}

function shouldFetchPosts(state, user) {
  const posts = state.postsByUser[user]
  if (!posts) {
    return true
  }
  if (posts.isFetching) {
    return false
  }
  return posts.didInvalidate
}

export function fetchPostsIfNeeded(user) {
  return (dispatch, getState) => {
    if (shouldFetchPosts(getState(), user)) {
      return dispatch(fetchPosts(user))
    }
  }
}

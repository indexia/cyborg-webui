import React, { Component, PropTypes } from 'react'
import { connect } from 'react-redux'
import { fetchUsers, fetchPostsIfNeeded, selectUser, invalidateUser } from '../actions'
import Picker from '../components/Picker'
import Posts from '../components/Posts'

class App extends Component {
  constructor(props) {
    super(props)
    this.handleChange = this.handleChange.bind(this)
    this.handleRefreshClick = this.handleRefreshClick.bind(this)
  }

  componentDidMount() {
    const { dispatch, selectedUser } = this.props
    dispatch(fetchUsers())
    dispatch(fetchPostsIfNeeded(selectedUser))
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.selectedUser !== this.props.selectedUser) {
      const { dispatch, selectedUser } = nextProps
      dispatch(fetchPostsIfNeeded(selectedUser))
    }
  }

  handleChange(nextUser) {
    this.props.dispatch(selectUser(nextUser))
  }

  handleRefreshClick(e) {
    e.preventDefault()

    const { dispatch, selectedUser } = this.props
    dispatch(invalidateUser(selectedUser))
    dispatch(fetchPostsIfNeeded(selectedUser))
  }

  render() {
    const { selectedUser, posts, isFetching, lastUpdated } = this.props
    const isEmpty = posts.length === 0
    return (
      <div>
        <Picker value={selectedUser}
                onChange={this.handleChange}
                options={[ 'dilbert', 'reactjs' ]} />
        <p>
          {lastUpdated &&
            <span>
              Last updated at {new Date(lastUpdated).toLocaleTimeString()}.
              {' '}
            </span>
          }
          {!isFetching &&
            <a href="#"
               onClick={this.handleRefreshClick}>
              Refresh
            </a>
          }
        </p>
        {isEmpty
          ? (isFetching ? <h2>Loading...</h2> : <h2>Empty.</h2>)
          : <div style={{ opacity: isFetching ? 0.5 : 1 }}>
              <Posts posts={posts} />
            </div>
        }
      </div>
    )
  }
}

App.propTypes = {
  selectedUser: PropTypes.string.isRequired,
  posts: PropTypes.array.isRequired,
  isFetching: PropTypes.bool.isRequired,
  lastUpdated: PropTypes.number,
  dispatch: PropTypes.func.isRequired
}

function mapStateToProps(state) {
  const { selectedUser, postsByUser } = state
  const {
    isFetching,
    lastUpdated,
    items: posts
  } = postsByUser[selectedUser] || {
    isFetching: true,
    items: []
  }

  return {
    selectedUser,
    posts,
    isFetching,
    lastUpdated
  }
}

export default connect(mapStateToProps)(App)
